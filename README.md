## 目的

* Spring 上でテストをするにあたってどのように DI を制御すればよいのかを調べる。

## 苦労したところ

* `@ContextConfiguration` で引数を省略した場合に、デフォルトのコンテキスト XML ファイルは `ClassName-context.xml` になるんだけど、最初読み込まれなくて困った。実際にはそのクラスがおかれている package と同一の package ディレクトリ以下におかないと読み込まれない (あたりまえといえばあたりまえ)。
* 本筋とは関係ない (DI の仕様の話) けど、 `@Autowired` で指定したフィールドは (あたりまえだけど) プロパティではなくフィールドなので XML で inject するコンポーネントを指定しようとしてもうまくいかない。
    * `@Autowired` だけではなく (lombok の) `@Setter` を指定しておくと上書き inject することができるようになる。
* 今は `sushiService` だけ XML に書いているけど、最初は下記のように inner bean として書いていたけどうまく動作しなかった。(もちろん当時は `SushiServiceTest` の `sushiService` フィールドは `@Setter` も追記していた)
    * 謎だったんだけど、ひょっとして `<property>` に `ref` をつけっぱなしだったかも。余力があればここも再検証する。

```xml
    <bean id="sushiServiceTest" class="com.example.dayflower.practice.spring4.test.SushiServiceTest">
        <property name="sushiService" ref="sushiService">
            <bean class="com.example.dayflower.practice.spring4.test.SushiService">
                <property name="fishService">
                    <bean class="com.example.dayflower.practice.spring4.test.FishServiceStub"></bean>
                </property>
            </bean>
        </property>
    </bean>
```

