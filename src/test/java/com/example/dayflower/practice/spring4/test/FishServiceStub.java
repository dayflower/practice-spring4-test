package com.example.dayflower.practice.spring4.test;

import java.util.Arrays;
import java.util.List;

public class FishServiceStub implements FishService {

	@Override
	public List<String> getFishes() {
		return Arrays.<String>asList("ニセマグロ", "ダイオウイカ", "たこつぼ");
	}

}
