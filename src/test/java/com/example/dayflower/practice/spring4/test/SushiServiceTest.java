package com.example.dayflower.practice.spring4.test;

import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration	//("SushiServiceTest-context.xml")
public class SushiServiceTest {

	@Autowired
	private SushiService sushiService;

	@Test
	public void getSushiTest() {
		List<String> sushi = sushiService.getSushi();
		log.debug("{}", sushi);
		assertThat(sushi, is(Arrays.<String>asList("ニセマグロ:白米", "ダイオウイカ:白米", "たこつぼ:白米")));
	}

}
