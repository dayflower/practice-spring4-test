package com.example.dayflower.practice.spring4.test;

public interface RiceService {
	public String getRice();
}
