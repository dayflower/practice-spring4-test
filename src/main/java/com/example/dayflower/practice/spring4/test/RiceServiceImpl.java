package com.example.dayflower.practice.spring4.test;

import org.springframework.stereotype.Service;

@Service
public class RiceServiceImpl implements RiceService {

	@Override
	public String getRice() {
		return "白米";
	}

}
